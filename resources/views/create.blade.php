<html>
<head>
	<title>Pertanyaan Create - CRUD</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
</head>
<body>

    <a class="btn btn-primary float-right mt-2" href="{{url('/pertanyaan')}}" role="button">Back</a>    

    <form action="/pertanyaan/" method="post">
        <div class="form-group">
            <label for="name">Judul</label>
            <input class="form-control" type="text" name="name" id="name" value="{{ old('judul') }}" placeholder="Masukkan Judul">
        </div>
        <div class="form-group">
            <label for="name">Isi</label>
            <input class="form-control" type="text" name="name" id="name" value="{{ old('isi') }}" placeholder="Masukkan Isi">
        </div>
 
        </div>
        <div class="form-group float-right">
            <button class="btn btn-lg btn-danger" type="reset">Reset</button>
            <button class="btn btn-lg btn-primary" type="submit">Submit</button>
        </div>
    </form>

</body>
</html>
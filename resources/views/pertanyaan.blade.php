<html>
<head>
	<title>Pertanyaan - CRUD</title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">
</head>
<body>

    <a class="btn btn-primary float-right mt-2" href="{{url('/pertanyaan/create')}}" role="button">Tambah Pertanyaan</a>    

    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Judul</th>
            <th>Isi</th>
            <th>Tanggal Dibuat</th>
            <th>Tanggal Diperbaharui</th>
            <th>Created at</th>
            <th>Updated at</th>
            <th width="280px">Action</th>
        </tr>

    <?php $i = 1; ?>
        @foreach($pertanyaans as $pertanyaan)
        <tr>
            <td class="text-center">{{$loop->iteration}}</td>
            <td>{{$pertanyaan->judul}}</td>
            <td>{{$pertanyaan->isi}}</td>
            <td class="text-center">{{$pertanyaan->tanggal_dibuat}}</td>
            <td class="text-center">{{$pertanyaan->tanggal_diperbaharui}}</td>
            <td class="text-center">{{$pertanyaan->created_at}}</td>
            <td class="text-center">{{$pertanyaan->updated_at}}</td>
            <td>
                <a href="{{url('/edit')}}" class="btn btn-xs btn-primary">Edit</a> |
                <a href="{{url('/delete')}}" class="btn btn-xs btn-danger">Delete</a>
            </td>
        </tr>
        @endforeach
    </table>

</body>
</html>
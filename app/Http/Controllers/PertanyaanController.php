<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PertanyaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // mengambil data dari table pertanyaans
        $pertanyaans = DB::table('pertanyaan')->get();
        // mengirim data pertanyaans ke view pertanyaans
        return view('pertanyaan', ['pertanyaans' => $pertanyaans]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('pertanyaan')->insert([
            'judul' => $request->judul,
            'isi' => $request->isi,
            'tanggal_dibuat' => $request->tanggal_dibuat,
            'tanggal_diperbaharui' => $request->tanggal_diperbaharui
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // mengambil data pertanyaan berdasarkan id yang dipilih
        $pertanyaan = DB::table('pertanyaan')->where('id',$id)->first();
        // passing data pertanyaan yang didapat ke view edit.blade.php
        return view('edit', compact('pertanyaan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('pertanyaan')->insert([
            'judul' => $request->judul,
            'isi' => $request->isi,
            'tanggal_dibuat' => $request->tanggal_dibuat,
            'tanggal_diperbaharui' => $request->tanggal_diperbaharui
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // menghapus data pertanyaan berdasarkan id yang dipilih
        DB::table('pertanyaan')->where('id', $id)->delete();
        // Alihkan ke halaman pertanyaan
        return redirect('/pertanyaan')->with('status', 'Data Pertanyaan Berhasil DiHapus');
    }
}
